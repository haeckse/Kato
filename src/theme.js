const katWhite = '#fff';
const katGreyLight = '#e6e6e6';
const katGrey = '#868686';
const katGreyMedium = '#383838';
const katGreyBrown = '#26211c';

export const lightTheme = {
  color: {
    h1: katGreyMedium,
    h3: katGrey,
    p: katGrey,
    bgBody: katGreyLight,

    card: {
      bg: katWhite,
      fg: katGrey,
      bgHover: '#f0f0f0',
      ledFg: katGreyLight,
      labelBg: katGreyLight,
      LabelFg: katGreyMedium,
      bgIconPlay: katGrey,
    },

    dropdown: {
      bg: katWhite,
      fg: katGrey,
      itemDisabled: katGreyLight,
    },

    langsel: {
      bg: katGreyLight,
      fg: katGrey,
      itemDisabled: katWhite,
    },

    menu: {
      bg: katWhite,
      item: katGrey,
      itemDisabled: katGreyLight,
    },
  },

  opacity: {
    flag: '0.7',
  },

  break: {
    horiz: {
      sm: '576px',
      lg: '768px',
    },
    vert: {
      sm: '480px',
      lg: '720px',
    },
  },
};

export const darkTheme = {
  color: {
    h1: katWhite,
    h3: katGrey,
    p: katGrey,
    bgBody: katGreyBrown,

    card: {
      bg: '#352e28',
      fg: katGreyLight,
      bgHover: '#3a342f',
      ledFg: '#3a3a3a',
      labelBg: katGrey,
      LabelFg: katGreyLight,
      bgIconPlay: '#554e48',
    },

    dropdown: {
      bg: katGreyBrown,
      fg: katGreyLight,
      itemDisabled: katGreyMedium,
    },

    langsel: {
      bg: katGreyLight,
      fg: katGrey,
      itemDisabled: katWhite,
    },

    menu: {
      bg: katWhite,
      item: katGrey,
      itemDisabled: katGreyLight,
    },
  },

  opacity: {
    flag: '0.85',
  },

  break: {
    horiz: {
      sm: '576px',
      lg: '768px',
    },
    vert: {
      sm: '480px',
      lg: '720px',
    },
  },
};
