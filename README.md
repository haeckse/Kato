# Kato

## Access hidden amp models in BOSS Katana MKII amps

Only five different amplifier types can be set via the Katana control panel. But the Katana has some 20 more amp models built in, which unfortunately cannot be accessed with the Tone Studio.

There are some editors out there that can access the additional models, like the 'Librarian' or 'Katana Man' app for Android and iOS devices.

Kato, on the other hand, is not intended as a full-fledged editor but serves only to be able to call up all 30 amp types. You can use it in parallel with Tone Studio but also stand alone.

## Running Kato

Kato works with Vivaldi, Chrome, Brave, Opera and Edge. When starting the App first time, the Browser may prompt you to enable MIDI-Mode, which is required to communicate with the Katana.

When the Katana is connected, the app shows a dialog for choosing a MIDI interface.

Once an amp model has been selected, it can simply be saved to a patch either via a longpress on the Katana control panel, or via the Tone Studio.

When using Kato in parallels with Tone Studio, launch the latter first.

## Hotkeys

Keyboard hotkeys for the panel buttons:

- (i)nfo
- (h)otkeys
- (x)elect MIDI interface
- (l)ight
- (s)olo
- (b)oost, (m)od, (f)x, (d)elay, (r)everb
- 0-9 switch channel

## License

[MIT](https://opensource.org/licenses/mit-license.php)
